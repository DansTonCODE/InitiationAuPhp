<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>dansTonCode-001</title>
</head>

<body>

  <?php
  try {
    $username = 'root';
    $password = '';
    $db = new PDO('mysql:host=localhost;dbname=test',  $username, $password);
  } catch (Exception $e) {
    var_dump($e);
    exit('Erreur avec la db');
  }
  ?>

  <form action="index.php" method="post">
    Ajout d'un aliment :
    NOM: <input type="text" name="nom"></input>
    COULEUR: <input type="text" name="couleur"></input>
    <input type="submit" value="Envoyer le nom"></input>
  </form>

  <?php


  if (isset($_POST['nom']) && isset($_POST['couleur'])) {
    try {
      $sql = 'INSERT INTO taliments (nom, couleur) VALUES (:nom,:couleur)';
      $request = $db->prepare($sql);
      $request->bindValue(':nom', $_POST['nom']);
      $request->bindValue(':couleur', $_POST['couleur']);
      $inserted = $request->execute();
    } catch (Exception $e) {
      var_dump($e);
      exit('Erreur avec la db');
    }
  }

  try {

    $sql = 'SELECT * FROM taliments WHERE id>0';
    $request = $db->query($sql);
    $datas = $request->fetchAll(PDO::FETCH_OBJ);
  ?>

    <table border="true">
      <?php

      foreach ($datas as $data) {
        $row = '<tr><td>' . $data->id . '</td><td>' . $data->nom  . '</td><td>' . $data->couleur  . '</td></tr>';
        echo ($row);
      }

      ?>
    </table>

  <?php

  } catch (Exception $e) {
    var_dump($e);
    exit('Erreur avec la db');
  }

  ?>

</body>

</html>