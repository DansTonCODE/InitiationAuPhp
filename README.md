# Initiation au PHP 🐘 {Dans Ton CODE} 

Code source de fin d'épisode de la [série de vidéos](https://www.youtube.com/playlist?list=PLncHDsbayrHurGIf2z7bpEBlB0Z0duiof) de la chaine YouTube [{Dans Ton CODE}](https://www.youtube.com/channel/UChpyq62kkR4me71EO6qXWhw) pour vous initier au développement web en PHP simplement et par la pratique :)

En partant de rien, créez votre propre application/site web vous-même !
