<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>dansTonCode-001</title>
</head>

<body>
  <?php
  try {
    $username = 'root';
    $password = '';
    $db = new PDO('mysql:host=localhost;dbname=test',  $username, $password);
  } catch (Exception $e) {
    var_dump($e);
    exit('Erreur avec la db');
  }

  $sessOK = false;

  $sql = 'SELECT * FROM tusers WHERE username LIKE :username';
  $request = $db->prepare($sql);
  $request->bindValue(':username', 'toto');
  $request->execute();
  $datas = $request->fetch(PDO::FETCH_OBJ);
  var_dump($datas);

  $password = 'totototoX';
  $gds = $datas->gds;
  $sha256 = $datas->sha256;

  $hash = hash('sha256', $password . $gds);
  var_dump($hash);

  if ($sha256 == $hash) {
    $sessOK = true;
  }

  if ($sessOK == true) {
  ?>

    <form action="index.php" method="post">
      Ajout d'un aliment :
      NOM: <input type="text" name="nom"></input>
      COULEUR: <input type="text" name="couleur"></input>
      <input type="text" name="action" value="create" hidden></input>
      <input type="submit" value="Envoyer le nom"></input>
    </form>

    <?php
    if (isset($_POST['action'])) {
      if ($_POST['action'] == 'create' && isset($_POST['nom']) && isset($_POST['couleur'])) {
        try {
          $sql = 'INSERT INTO taliments (nom, couleur) VALUES (:nom,:couleur)';
          $request = $db->prepare($sql);
          $request->bindValue(':nom', $_POST['nom']);
          $request->bindValue(':couleur', $_POST['couleur']);
          $inserted = $request->execute();
          header("Location: index.php");
        } catch (Exception $e) {
          var_dump($e);
          exit('Erreur avec la db');
        }
      } else if ($_POST['action'] == 'delete') {
        try {
          $sql = 'DELETE FROM taliments WHERE id = :id';
          $request = $db->prepare($sql);
          $request->bindValue(':id', $_POST['id']);
          $inserted = $request->execute();
          header("Location: index.php");
        } catch (Exception $e) {
          var_dump($e);
          exit('Erreur avec la db');
        }
      } else if ($_POST['action'] == 'edit' && isset($_POST['nom']) && isset($_POST['couleur']) && isset($_POST['id'])) {
        try {
          $sql = 'UPDATE taliments SET nom = :nom, couleur = :couleur WHERE id = :id';
          $request = $db->prepare($sql);
          $request->bindValue(':id', $_POST['id']);
          $request->bindValue(':nom', $_POST['nom']);
          $request->bindValue(':couleur', $_POST['couleur']);
          $inserted = $request->execute();
          header("Location: index.php");
        } catch (Exception $e) {
          var_dump($e);
          exit('Erreur avec la db');
        }
      }
    }

    try {
      $sql = 'SELECT * FROM taliments WHERE id>0';
      $request = $db->query($sql);
      $datas = $request->fetchAll(PDO::FETCH_OBJ);
    ?>
      <table border="true">
        <?php
        foreach ($datas as $data) {
          $row = '<tr><form action="index.php" method="post"><td>' . $data->id . '</td><td><input type="text" value="' . $data->nom  . '" name="nom"></input></td><td><input type="text" value="' . $data->couleur  . '" name="couleur"></input></td><td><input type="text" name="action" value="edit" hidden></input><input type="text" name="id" value="' . $data->id . '" hidden></input><input type="submit" value="Editer"></input></form><form action="index.php" method="post"><input type="text" name="action" value="delete" hidden></input><input type="text" name="id" value="' . $data->id . '" hidden></input><input type="submit" value="Suppr."></input></form></td></tr>';
          echo ($row);
        }
        ?>
      </table>
  <?php
    } catch (Exception $e) {
      var_dump($e);
      exit('Erreur avec la db');
    }
  }
  ?>
</body>

</html>